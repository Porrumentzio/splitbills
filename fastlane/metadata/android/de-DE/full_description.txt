Nutze SplitBills um Ausgaben mit deinen Freunden oder andern Menschen zu 
teilen. Du kannst Gruppen erstellen und Ausgaben zu diesen hinzufügen 
und dann deine Bilanzen ansehen. SplitBills arbeitet ohne einen 
zentralen Server oder irgendeiner Registration. SplitBills ist freie 
Software. Deine Ausgaben sollten dir gehören!

Wenn du irgendwelche Probleme mit SplitBills hast, dann gebe der App 
nicht gleich eine Ein-Stern Bewertung sondern kontaktiere mich oder mach 
einen Bug auf: https://gitlab.com/flexw/splitbills/issues

Features:

* Teile Ausgaben mit Freunden
* Syncronisiere die Ausgaben mit deinen Freunden
* Seh dir deine Bilanz an
* Keine Registrierung notwendig
* Dezentral

(Source Code: https:gitlab.com/flexw/splitbills)